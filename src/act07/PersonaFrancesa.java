package act07;

/**
 *
 * @author sergio
 */
public class PersonaFrancesa extends Persona {

    public PersonaFrancesa(String nombre) {
        super(nombre);
    }
    
    @Override
    public void saluda() {
        System.out.println("Bonjour, je suis " + this.nombre);
    }
}
