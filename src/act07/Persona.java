
package act07;

/**
 *
 * @author sergio
 */
public class Persona {
    protected String nombre;
    private String DNI;
    
    public Persona(String nombre) {
        this.nombre = nombre;
    }
    public void saluda() {
        System.out.println("Hola, soy " + this.nombre);
    }    
    
    public String getDNI() {
        return this.DNI;
    }
}
