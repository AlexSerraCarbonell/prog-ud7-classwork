package act07;

/**
 *
 * @author sergio
 */
public class PersonaRusa extends Persona {

    public PersonaRusa(String nombre) {
        super(nombre);
    }
    @Override
    public void saluda() {
        System.out.println("Привет, йоу " + this.nombre);
    }
}
