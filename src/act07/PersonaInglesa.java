package act07;

/**
 *
 * @author sergio
 */
public class PersonaInglesa extends Persona {
    private String passaport;

    public PersonaInglesa(String nombre) {
        super(nombre);
    }
       
    @Override
    public void saluda() {
        System.out.println("Hi, I'm " + this.nombre);
    }
    
    public void setPassaport(String passaport) {
        this.passaport = passaport;
    }
    
    public void mostrarPassaport() {
        System.out.println("My passport is: " + this.passaport);
    }
}
