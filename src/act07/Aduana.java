/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package act07;

/**
 *
 * @author sergio
 */
public class Aduana {

    Persona[] personas;

    private final int CAPACIDAD_MAX = 3;

    public Aduana() {
        this.personas = new Persona[CAPACIDAD_MAX];
    }

    private void hablar(String mensaje) {
        System.out.println("ADUANA: " + mensaje);
    }

    public void entrar(Persona persona) {
        this.hablar("Welcome!");
        persona.saluda();
        
        int numPersonas = this.numPersonas();
        if (numPersonas == CAPACIDAD_MAX) {
            this.hablar("Sorry. You can not enter.");
        } else {
            personas[numPersonas] = persona;
            if (persona instanceof PersonaInglesa) {
                this.hablar("Could you show me your passport, please.");
                ((PersonaInglesa) persona).mostrarPassaport();
            }
        }
        System.out.println("------");
    }

    private int numPersonas() {
        int numPersonas = 0;
        for (Persona p : this.personas) {
            if (p == null) {
                return numPersonas;
            }
            numPersonas++;
        }
        return numPersonas;
    }
}
