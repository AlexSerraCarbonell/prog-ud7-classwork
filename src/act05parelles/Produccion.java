package act05parelles;

/**
 *
 * @author sergio
 */
public class Produccion {

    private String titulo;
    private Formato formato;
    private int duracion;
    private String descripcion;
    private Data fechaLanzamiento;

    public Produccion(String titulo, Formato formato, int duracion, Data fechaLanzamiento) {
        this.titulo = titulo;
        this.formato = formato;
        this.duracion = duracion;
        this.fechaLanzamiento = fechaLanzamiento;
    }

    protected String getDuracionHorasMinutosSegundos() {
        int horas = this.duracion / 3600;
        int min = (this.duracion % 3600) / 60;
        int seg = (this.duracion % 3600 % 60);
        return String.format("%dh %dmin %ds", horas, min, seg);
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public void mostrarDetalle() {
        System.out.printf("%s (%d)%n", this.titulo, this.fechaLanzamiento.getAnyo());
        System.out.println("Descripción: " + this.descripcion);
    }

    @Override
    public String toString() {
        return "Produccion: " + "título = " + this.titulo + ", duracion = "
                + getDuracionHorasMinutosSegundos() + ", formato = " + this.formato;
    }

}
