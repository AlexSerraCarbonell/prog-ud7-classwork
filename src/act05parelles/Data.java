package act05parelles;

public class Data {   
    private int dia;
    private int mes;
    private int anyo;

    public Data() {
        this(1, 1, 1970);
    }

    public Data(int dia, int mes, int anyo) {
       this.dia = dia;
       this.mes = mes;
       this.anyo = anyo;
    }   

    public int getDia() {
        return dia;
    }
   
    public int getMes() {
        return mes;
    }
    
    public int getAnyo() {
        return anyo;
    }  

    @Override
    public String toString() {
        return dia + "/" + mes + "/" + anyo;
    }

}
