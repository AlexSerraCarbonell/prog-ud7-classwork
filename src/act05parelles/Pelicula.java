package act05parelles;

/**
 *
 * @author sergio
 */
public class Pelicula extends Produccion {

    private String actor;
    private String actriz;

    public Pelicula(String titulo, Formato formato, int duracion, Data fechaLanzamiento, String actor, String actriz) {
        super(titulo, formato, duracion, fechaLanzamiento);
        this.actor = actor;
        this.actriz = actriz;
    }

    public Pelicula(String titulo, Formato formato, Data fechaLanzamiento, String actor, String actriz) {
        super(titulo, formato, 4800, fechaLanzamiento);
        this.actor = actor;
        this.actriz = actriz;
    }
    
    @Override
    public void mostrarDetalle() {
        System.out.println("----------------- Película ----------------");
        super.mostrarDetalle();
        System.out.println("Duración: " + super.getDuracionHorasMinutosSegundos());
        System.out.println("--------------------------------------------");
    }
    
    @Override
    public String toString() {
        return super.toString() + " (película)" + ", actor = " + this.actor + ", actriz  = " + this.actriz;
    }
    
    

}
