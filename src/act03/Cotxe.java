package act03;

/**
 *
 * @author sergio
 */
public class Cotxe extends Vehicle {
    protected float carburant;
    private String matricula;
    
    public Cotxe(int velocitat, float carburant) {
        super(velocitat);
        this.carburant = carburant;
    }
    
    public void accelerar() {
        super.accelerar();
        this.carburant -= 0.5;
        System.out.println("Accelerant en COTXE");
    }
    
    public void proveir(int quantitat) {
        this.carburant += quantitat;                
    }
    
    public float getCarburant() {
        return this.carburant;
    }
}
