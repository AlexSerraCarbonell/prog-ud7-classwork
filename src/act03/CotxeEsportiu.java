package act03;

/**
 *
 * @author sergio
 */
public class CotxeEsportiu extends Cotxe {
    private boolean descapotable;

    public CotxeEsportiu(int velocitat, float carburant) {
        super(velocitat, carburant);
        this.descapotable = true;
    }
    
    @Override
    public void accelerar() {
        super.accelerar();
        this.carburant -= 1;
        System.out.println("Accelerant en ESPORTIU");
    }

    

    
}
