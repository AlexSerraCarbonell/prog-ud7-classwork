package act03;

/**
 *
 * @author sergio
 */
public class TestCotxe {
    public static void main(String[] args) {
        Cotxe c = new Cotxe(120, 10);
        CotxeEsportiu ce = new CotxeEsportiu(130, 10);
        
        c.proveir(50);
        ce.proveir(50);
        
        c.accelerar();
        ce.accelerar();
        
        System.out.println("Carburant cotxe: "+ c.getCarburant());
        System.out.println("Carburant esportiu: "+ ce.getCarburant());
    }
}
