package act09y10;

import act09y10.enums.Size;
import act09y10.types.Animal;
import act09y10.types.Cat;
import act09y10.types.Dog;
import act09y10.types.Hippo;
import act09y10.types.Horse;
import act09y10.types.Lion;
import act09y10.types.Tiger;
import act09y10.types.Wolf;
import java.util.Objects;

public class TestAnimal {

    public static void main(String[] args) {

        Animal[] animals = new Animal[10];
        animals[0] = new Lion(Size.BIG, "La Sabana");
        animals[1] = new Wolf(Size.BIG, "La Sabana");
        animals[2] = new Wolf(Size.MEDIUM, "La Serreta");
        animals[3] = new Dog(Size.SMALL, "Alcoy");
        animals[4] = new Cat(Size.SMALL, "Spain");
        animals[5] = new Cat(Size.SMALL, "Spain");
        animals[6] = new Horse(Size.BIG, "Spain");
        animals[7] = new Hippo(Size.BIG, "La Sabana");
        animals[8] = new Tiger(Size.BIG, "África");
        animals[9] = new Tiger(Size.BIG, "África");

        System.out.println("---- Antes de vacunarlos y alimentarlos ------");
        for (Animal animal : animals) {
            System.out.println(animal);
        }
        System.out.println();

        Vet vet = new Vet();
        for (Animal animal : animals) {
            vet.vaccinate(animal);
            vet.feed(animal);
        }

        System.out.println();
        System.out.println("---- Después de vacunarlos y alimentarlos ------");
        for (Animal animal : animals) {
            System.out.println(animal);
        }
        
        System.out.println("Son iguales? " + animals[0].equals(animals[1]));
        System.out.println("Son iguales? " + animals[0].equals(animals[2]));
        System.out.println("Son iguales? " + animals[0].equals(animals[9]));
        System.out.println("Son iguales? " + Objects.equals(animals[0], animals[9]));
        

    }

}
