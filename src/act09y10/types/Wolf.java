package act09y10.types;

import act09y10.enums.Food;
import act09y10.enums.Size;


public class Wolf extends Animal {

    private static final String NOMBRE = "Lobo";
    public Wolf(Size size, String location) {
        super(Food.OMNIVORE, size, location);
    }

    @Override
    protected String getNombre() {
        return NOMBRE;
    }

    @Override
    public void makeNoise() {
        System.out.println("AUUUUUUUHHHH!!!!!!!!!");
    }
}
