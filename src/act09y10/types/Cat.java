package act09y10.types;

import act09y10.enums.Food;
import act09y10.enums.Size;


public class Cat extends Animal{

    private static String NOMBRE = "Gato";
    
    public Cat(Size size, String location) {
        super(Food.CARNIVOROUS, size, location);
    }

    @Override
    protected String getNombre() {
        return NOMBRE;
    }

    @Override
    public void makeNoise() {
        System.out.println("Miauuuu!!!!!!!!!");
    }
}
