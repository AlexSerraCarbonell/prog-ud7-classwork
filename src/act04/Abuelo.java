package act04;

/**
 *
 * @author sergio
 */
public class Abuelo {

    private String name;
    
    public Abuelo() {
        this.name = "SinNombre";
    }

    public Abuelo(String name) {
        this.name = name;
    }

    public String toString() {
        return "name: " + name;
    } 
}
