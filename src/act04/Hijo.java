package act04;

/**
 *
 * @author sergio
 */
public class Hijo extends Padre {

    private String surname;

    public Hijo(String surname) {
        this.surname = surname;
    }

    @Override
    public String toString() {
        return super.toString() + ", surname: " + surname;
    }
}
