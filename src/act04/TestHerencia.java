package act04;

/**
 *
 * @author sergio
 */
public class TestHerencia {

    public static void main(String[] args) {

        Hijo hijo = new Hijo("Hijo");
        Padre padre = new Padre("Padre");
        Abuelo abuelo = new Abuelo("Abuelo");
        
        System.out.println("Objeto hijo -> " + hijo.toString());
        System.out.println("Objeto padre -> " + padre.toString());
        System.out.println("Objecto abu -> " + abuelo.toString());

    }
}
