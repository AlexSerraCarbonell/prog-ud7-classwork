package act04;

/**
 *
 * @author sergio
 */
public class Padre extends Abuelo {

    public Padre() {
    }

    public Padre(String name) {        
        super(name);
    }

    @Override
    public String toString() {
        return "padre " + super.toString();
    }
}
