
package act01;

/**
 *
 * @author sergio
 */
public class TestPersona {
    public static void main(String[] args) {
        PersonaFrancesa pf = new PersonaFrancesa();
        PersonaInglesa pi = new PersonaInglesa();
        PersonaRusa pr = new PersonaRusa();
        PersonaOtra po = new PersonaOtra();
        
        pf.saluda();
        pi.saluda();
        pr.saluda();
        po.saluda();
        System.out.println(po.getDNI());
    }
}
